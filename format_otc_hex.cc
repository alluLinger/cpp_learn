#include <iostream>
#include <stdio.h>

int main(){
    int x = 100;
    printf("dec = %d, oct = %o, hex = %x\n", x,x,x);
    printf("dec = %d, oct = %#o, hex = %#x\n", x,x,x);

    using namespace std;
    cout << "dec = " << x << endl;
    cout << hex;
    cout << "hex = " << x << endl;
    cout << oct;
    cout << "otc = " << x << endl;
    return 0;
}
