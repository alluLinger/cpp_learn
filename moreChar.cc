#include <iostream>

int main(int argc, char *argv[])
{
    using namespace std;
    char ch = 'M';
    int i = ch;
    cout << "THe ASCII code for " << ch << " is " << i << endl;
    
    cout << "+1 to char code: " << endl;
    ch = ch + 1;
    i = ch;
    cout << "THe ASCII code for " << ch << " is " << i << endl;
    
    //using cout.put()
    cout << "cout.put()" << endl;
    cout.put(ch);
    cout.put(i); // still a char
    cout.put('i');
    //using cout << 
    cout << "\ncout << buff" << endl;
    cout << ch;
    cout << i; // int
    cout << 'i';
    
    int a\u00E2 = 100;
    cout << "\n a\u00E2" << endl;

    cout << endl;
    wcout << L"tall" << endl;
    wchar_t bob = L'P';
    wcout << bob << endl;

    char16_t ch1 = u'q';
    char32_t ch2 = U'你';
    char32_t ch3 = U'\U0000222B';
    //u can't just pring it
    cout << ch1 << ch2 << ch3 << endl;

    return 0;
}
