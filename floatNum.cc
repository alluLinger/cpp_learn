#include <iostream>
#include <float.h>

int main(int argc, char *argv[])
{
    using namespace std;
    cout.setf(ios_base::fixed, ios_base::floatfield);
    float tub = 10.0 / 3.0;
    float f = 1.00000f;
    cout << "tub = " << tub << endl;
    cout << "f = " << f << endl;
    cout << "float.h FLT_MAX_10_EXP = " << FLT_MAX_10_EXP << endl;
    return 0;
}
